
/** \file MWLS_Approx.cpp
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#define NDEBUG 1
#define BOOST_UBLAS_NDEBUG 1
// #include <moab/BVHTree.hpp>
#include <moab/SpatialLocator.hpp>
#include <moab/ElemEvaluator.hpp>
#include <MoFEM.hpp>

#ifdef WITH_METAIO
  #include <metaImage.h>
#endif

using namespace MoFEM;
#include <cholesky.hpp>
#include <MWLSApprox.hpp>

namespace MWLSApproxSpace {

 PetscErrorCode MovingLeastSquares::getMWLSOptions() {

   MoFEMFunctionBeginHot;
   ierr = PetscOptionsBegin(
     mField.get_comm(),"","Get options for moving least square approximation","none"
   ); CHKERRQ(ierr);
   ierr = PetscOptionsScalar(
     "-mwls_dm",
     "edge length scaling for influence radius","",
     dmFactor,&dmFactor,PETSC_NULL
   ); CHKERRQ(ierr);
   ierr = PetscOptionsInt(
     "-mwls_number_of_base_functions",
     "1 = constant, 4 = linear, 10 = quadratic approximation","",
     nbBasePolynomials,&nbBasePolynomials,PETSC_NULL
   ); CHKERRQ(ierr);

   #ifdef WITH_METAIO
   trimInfluenceNodes = PETSC_FALSE;
   ierr = PetscOptionsBool("-trim","if true trim nodes","",trimInfluenceNodes,&trimInfluenceNodes,PETSC_NULL); CHKERRQ(ierr);
   sCaleMHD = 1;
   ierr = PetscOptionsScalar(
     "-scaleMHD",
     "scale the distance between voxels for MHD file (eg. from mm to m)","",
     sCaleMHD,&sCaleMHD,PETSC_NULL
    ); CHKERRQ(ierr);
   tHresholdMHD = -1024;
   ierr = PetscOptionsScalar(
     "-threshold",
     "don't take into consideration voxels below this value","",
     tHresholdMHD,&tHresholdMHD,PETSC_NULL
    ); CHKERRQ(ierr);

    paramDensity[0] = 1;
    paramDensity[1] = 0;
    int n_max = 2;
    ierr = PetscOptionsGetScalarArray(PETSC_NULL,"-param_density",paramDensity,&n_max,PETSC_NULL
  ); CHKERRQ(ierr);

    #endif // WITH_METAIO

   ierr = PetscOptionsEnd(); CHKERRQ(ierr);
   MoFEMFunctionReturnHot(0);
 }

 PetscErrorCode MovingLeastSquares::loadMWLSMesh(std::string file_name, bool med_flg) {


   MoFEMFunctionBeginHot;

   ierr = getMWLSOptions(); CHKERRQ(ierr);

   const char *option;
   option = "";
   rval = mwlsMoab.load_file(file_name.c_str(),0,option); CHKERRQ_MOAB(rval);
   // rval = mwlsMoab.get_entities_by_type(0,MBVERTEX,levelNodes); CHKERRQ_MOAB(rval);
   rval = mwlsMoab.get_entities_by_dimension(0,3,levelTets); CHKERRQ_MOAB(rval);
   if(levelTets.empty()) {
     SETERRQ(mField.get_comm(),MOFEM_DATA_INCONSISTENCY,"No 3d element in MWLS mesh");
   }

   rval = mwlsMoab.get_adjacencies(levelTets,1,true,levelEdges,moab::Interface::UNION); CHKERRQ_MOAB(rval);

   if(med_flg) {
      // Create HO node on tetrahedral
      EntityHandle meshset;
      rval = mwlsMoab.create_meshset(MESHSET_SET,meshset); CHKERRQ_MOAB(rval);
      rval = mwlsMoab.add_entities(meshset,levelTets); CHKERRQ_MOAB(rval);
      rval = mwlsMoab.convert_entities(meshset,false,false,true); CHKERRQ_MOAB(rval);
      rval = mwlsMoab.delete_entities(&meshset,1); CHKERRQ_MOAB(rval);
  } else {
      Range tets;
      rval = mwlsMoab.get_entities_by_type(0,MBVERTEX,tets); CHKERRQ_MOAB(rval);
      // rval = mwlsMoab.get_entities_by_type(0,MBVERTEX,levelNodes); CHKERRQ_MOAB(rval);
      CHKERR mwlsMoab.get_connectivity(tets, levelNodes, true);
      this->mhdRhoFile = PETSC_FALSE;
  }

   // Store edge nodes coordinates in FTensor
   double edge_node_coords[6];
   FTensor::Tensor1<double*,3> t_node_edge[2] = {
     FTensor::Tensor1<double*,3>(edge_node_coords,&edge_node_coords[1],&edge_node_coords[2]),
     FTensor::Tensor1<double*,3>(&edge_node_coords[3],&edge_node_coords[4],&edge_node_coords[5])
   };

   // Get edge lengths
   maxEdgeL = 0;
   for(Range::iterator eit=levelEdges.begin();eit!=levelEdges.end();eit++) {
     int num_nodes;
     const EntityHandle* conn;
     rval = mwlsMoab.get_connectivity(*eit,conn,num_nodes,true); CHKERRQ_MOAB(rval);
     rval = mwlsMoab.get_coords(conn,num_nodes,edge_node_coords); CHKERRQ_MOAB(rval);
     t_node_edge[0](i) -= t_node_edge[1](i);
     double l = sqrt(t_node_edge[0](i)*t_node_edge[0](i));
     maxEdgeL = (maxEdgeL<l) ? l : maxEdgeL;
   }
  
  if(med_flg) {
   // Get HO nodes in TET center
   std::vector<double> edge_length;
   for(Range::iterator tit =levelTets.begin();tit!=levelTets.end();tit++) {
     int num_nodes;
     const EntityHandle* conn;
     rval = mwlsMoab.get_connectivity(*tit,conn,num_nodes,false); CHKERRQ_MOAB(rval);
     EntityHandle ho_node;
     EntityType tit_type = mwlsMoab.type_from_handle(*tit);
     rval = mwlsMoab.high_order_node(*tit,conn,tit_type,ho_node); CHKERRQ_MOAB(rval);
     levelNodes.insert(ho_node);
   }
  }
   // Create tree and find maximal edge length. Maximal edge length will is used
   // to find neighbours nodes to material point.
   myTree = boost::shared_ptr<AdaptiveKDTree>(new AdaptiveKDTree(&mwlsMoab));
   PetscPrintf(mField.get_comm(),"Build tree ... ");
   rval = myTree->build_tree(levelTets,&treeRoot); CHKERRQ_MOAB(rval);
  //  if(mwlsMoab.type_from_handle(levelTets[0])==MBTET) {
  //    boost::shared_ptr<ElemEvaluator> elemEval(myTree,new ElemEvaluator(&mwlsMoab));
  //    myTree->set_eval(elemEval.get());
  //  }
   PetscPrintf(mField.get_comm(),"done\n");
   // myTree->print();

   MoFEMFunctionReturnHot(0);
 }

 PetscErrorCode MovingLeastSquares::getValuesToNodes(Tag th) {

   MoFEMFunctionBeginHot;
   int length;
   rval = mwlsMoab.tag_get_length(th,length); CHKERRQ_MOAB(rval);
   double vals[length];
   bzero(vals,length*sizeof(double));
   // clean tag on nodes
   rval = mwlsMoab.tag_clear_data(th,levelNodes,vals); CHKERRQ_MOAB(rval);
   for(Range::iterator tit = levelTets.begin();tit!=levelTets.end();tit++) {
     rval = mwlsMoab.tag_get_data(th,&*tit,1,vals); CHKERRQ_MOAB(rval);
     int num_nodes;
     const EntityHandle* conn;
     rval = mwlsMoab.get_connectivity(*tit,conn,num_nodes,false); CHKERRQ_MOAB(rval);
     EntityHandle ho_node;
     EntityType tit_type = mwlsMoab.type_from_handle(*tit);
     rval = mwlsMoab.high_order_node(*tit,conn,tit_type,ho_node); CHKERRQ_MOAB(rval);
     // double coords[3];
     // rval = mwlsMoab.get_coords(&ho_node,1,coords); CHKERRQ_MOAB(rval);
     // vals[0] = coords[0]*coords[0]*coords[0];
     rval = mwlsMoab.tag_set_data(th,&ho_node,1,vals); CHKERRQ_MOAB(rval);
   }
   MoFEMFunctionReturnHot(0);
 }

 PetscErrorCode MovingLeastSquares::getBox(
   double material_coords[3],
   std::vector<double>& coords
 ) {
   MoFEMFunctionBeginHot;
   FTensor::Tensor1<double,3> t_material_coords(
     material_coords[0],material_coords[1],material_coords[2]
   );
   FTensor::Tensor1<double*,3> t_coords(
     &coords[0],&coords[1],&coords[2],3
   );
   FTensor::Index<'i',3> i;
   double max_xyz[] = {0,0,0};
   for(int bb = 0;bb!=coords.size()/3;bb++) {
     t_coords(i) -= t_material_coords(i);
     for(int dd = 0;dd!=3;dd++) {
       max_xyz[dd] = (max_xyz[dd]<fabs(t_coords(dd))) ? fabs(t_coords(dd)) : max_xyz[dd];
     }
     ++t_coords;
   }
   boxSize = (max_xyz[0]<max_xyz[1]) ? max_xyz[0] : max_xyz[1];
   boxSize = (boxSize<max_xyz[2]) ? boxSize : max_xyz[2];
   MoFEMFunctionReturnHot(0);
 }

 PetscErrorCode MovingLeastSquares::getInfluenceNodes(
   double material_coords[3],bool search_point
 ) {
   MoFEMFunctionBeginHot;

   // Fining neighbours edge nodes
   Range tree_tets;
   Range nodes;
   std::vector<double> coords;

   if(myTree) {
     MeshTopoUtil mtu(&mwlsMoab);
     if(search_point) {
       // Find TET class to location
       CartVect params(material_coords);
       EntityHandle leaf_out;
       rval = myTree->point_search(
         material_coords,leaf_out,maxEdgeL*1e-5,maxEdgeL*1e-3,NULL,NULL,&params
       ); CHKERRQ_MOAB(rval);
       if(mwlsMoab.type_from_handle(leaf_out)==MBENTITYSET) {
         rval = mwlsMoab.get_entities_by_dimension(leaf_out,3,tree_tets,true); CHKERRQ_MOAB(rval);
       } else {
         // Get TETS in neighbourhood
         rval = mtu.get_bridge_adjacencies(leaf_out,0,3,tree_tets); CHKERRQ_MOAB(rval);
       }
       // Get bounding box
       boxSize = 0;
       rval = mwlsMoab.get_connectivity(tree_tets,nodes,true); CHKERRQ_MOAB(rval);
       coords.resize(3*nodes.size());
       rval = mwlsMoab.get_coords(nodes,&coords[0]); CHKERRQ_MOAB(rval);
       ierr = getBox(material_coords,coords); CHKERRQ_MOAB(rval);
     } else {
       boxSize = 0;
       rval = mwlsMoab.get_connectivity(tree_tets,nodes,true); CHKERRQ_MOAB(rval);
       coords.resize(3*nodes.size());
       rval = mwlsMoab.get_coords(nodes,&coords[0]); CHKERRQ_MOAB(rval);
       ierr = getBox(material_coords,coords); CHKERRQ_MOAB(rval);
     }
   } else {
     SETERRQ(mField.get_comm(),MOFEM_DATA_INCONSISTENCY,"kd-three not build");
   }

   if(nodes.empty()) {
     SETERRQ(mField.get_comm(),MOFEM_DATA_INCONSISTENCY,"Node outside region of internal stress volume");
   }

   const double dm = maxEdgeL*dmFactor; // Influence radius
   while(boxSize<dm) {
     rval = mwlsMoab.get_adjacencies(
       nodes,3,false,tree_tets,moab::Interface::UNION
     ); CHKERRQ_MOAB(rval);
     rval = mwlsMoab.get_connectivity(tree_tets,nodes,true); CHKERRQ_MOAB(rval);
     coords.resize(3*nodes.size());
     rval = mwlsMoab.get_coords(nodes,&coords[0]); CHKERRQ_MOAB(rval);
     boxSize = 0;
     ierr = getBox(material_coords,coords); CHKERRQ_MOAB(rval);
     if (nodes.size() > nbBasePolynomials) break;
   }

   influenceNodes.clear();
   rval = mwlsMoab.get_connectivity(tree_tets,nodes,false); CHKERRQ_MOAB(rval);
   influenceNodes = intersect(levelNodes,nodes);

   PetscFunctionReturn(0);
 }

 #ifdef WITH_METAIO

 PetscErrorCode MovingLeastSquares::loadMHData(std::string file_name) {
  PetscFunctionBegin;
  getMWLSOptions();

  metaFile = boost::shared_ptr<MetaImage>(new MetaImage(file_name.c_str()));
  maxEdgeL = (metaFile->ElementSize()[0] + metaFile->ElementSize()[1] + metaFile->ElementSize()[2])/3.0 * sCaleMHD;
  this->mhdRhoFile = PETSC_TRUE;

  //moab_testing
  // moab_testing = &mb_testing;
  PetscFunctionReturn(0);
 }

 PetscErrorCode MovingLeastSquares::getInfluenceNodesFromMeta(
   double material_coords[3]
 ) {
   PetscFunctionBegin;

   const double dm = maxEdgeL*dmFactor; // Influence radius
   
   const double *element_size = metaFile->ElementSize();
   const int *dim_size = metaFile->DimSize();

   //make a loop
   double rx = ceil(dm / (element_size[0] * sCaleMHD));
   double ry = ceil(dm / (element_size[1] * sCaleMHD));
   double rz = ceil(dm / (element_size[2] * sCaleMHD));

   double ix = ceil( material_coords[0] / (element_size[0] * sCaleMHD) );
   double iy = ceil( material_coords[1] / (element_size[1] * sCaleMHD) );
   double iz = ceil( material_coords[2] / (element_size[2] * sCaleMHD) );

   vector<int> vec_ix(2*rx);
   vector<int> vec_iy(2*ry);
   vector<int> vec_iz(2*rz);

  int ii = 0;
  for(int i = 0;i<2*rx;i++) {                         // vectors of coordinates of points within given cube
    int idx = ix-rx+i;
    if(idx >= dim_size[0] || idx < 0) continue;       // erasing indices beyond the border
    vec_ix[ii++] = idx;
  }
  vec_ix.resize(ii);
  ii = 0;
  for(int i = 0;i<2*ry;i++) {
    int idx = iy-ry+i;
    if(idx >= dim_size[1] || idx < 0) continue;       // erasing indices beyond the border
    vec_iy[ii++] = idx;
  }
  vec_iy.resize(ii);
  ii = 0;
  for(int i = 0;i<2*rz;i++) {
    int idx = iz-rz+i;
    if(idx >= dim_size[2] || idx < 0) continue;       // erasing indices beyond the border
    vec_iz[ii++] = idx;
  }
  vec_iz.resize(ii);


  influenceIndicesData.clear();
   for(vector<int>::iterator it_iz = vec_iz.begin(); it_iz != vec_iz.end();++it_iz) {
     int a = (*it_iz) * dim_size[0] * dim_size[1];
    //  double dz2 = (*it_iz*(element_size[2]*sCaleMHD) - material_coords[2]) * (*it_iz*(element_size[2]*sCaleMHD) - material_coords[2]);
     for(vector<int>::iterator it_iy = vec_iy.begin(); it_iy != vec_iy.end();++it_iy) {
       int b = (*it_iy) * dim_size[0];
      //  double dy2 = (*it_iy*(element_size[1]*sCaleMHD) - material_coords[1]) * (*it_iy*(element_size[1]*sCaleMHD) - material_coords[1]);
       for(vector<int>::iterator it_ix = vec_ix.begin(); it_ix != vec_ix.end();++it_ix) {
        // influenceIndicesDistance[ii] =  sqrt( dz2 + dy2 + (*it_ix*(element_size[0]*sCaleMHD) - material_coords[0])
        // * (*it_ix*(element_size[0]*sCaleMHD) - material_coords[0]) );
        const unsigned int index = a + b + (*it_ix);
        const double v_data = metaFile->ElementData(index) * paramDensity[0] + paramDensity[1];
        // double dist =  sqrt( dz2 + dy2 + (*it_ix*(element_size[0]*sCaleMHD) - material_coords[0])
        // * (*it_ix*(element_size[0]*sCaleMHD) - material_coords[0]) );
        if (v_data < tHresholdMHD) continue;
            const double xx = *it_ix * element_size[0] * sCaleMHD;
            const double yy = *it_iy * element_size[1] * sCaleMHD;
            const double zz = *it_iz * element_size[2] * sCaleMHD;
            influenceIndicesData.push_back(VoxelData(xx,yy,zz,v_data));

        }
      }
    }
   PetscFunctionReturn(0);
  }

  PetscErrorCode MovingLeastSquares::calculateApproxFunMHD(
    double material_coords[3]
  ) {
    PetscFunctionBegin;
    const double dm = maxEdgeL*dmFactor; // influence radius

    // Node coordinates
    FTensor::Tensor1<double,3> t_material_coords(
      material_coords[0],material_coords[1],material_coords[2]
    );

      //   int jj = 0;
      if(this->trimInfluenceNodes) {
        for(list<VoxelData>::iterator it = influenceIndicesData.begin();
        it != influenceIndicesData.end();) {
          const double coords[3] = {(*it).cOords[0],(*it).cOords[1],(*it).cOords[2]};
          double ray_dir[3] = {(double)rand(),(double)rand(),(double)rand()};
          // EntityHandle triangle;
          // rval = this->kd_tree->closest_triangle(
          //   this->kdTreeRootMeshset,
          //   coords,
          //   ray_dir,
          //   triangle
          // ); CHKERRQ_MOAB(rval);
          // cblas_daxpy(3,-1,coords,1,ray_dir,1);       //closest point is now ray direction
          double norm = cblas_dnrm2(3,ray_dir,1);
          cblas_dscal(3,1./norm,ray_dir,1);
          const double tol = 1e-6; //
          // if (norm < tol){
          //   it++;
          //   continue;
          // }  // distance close to zero so point is on the edge, keep it

          // for(int dd=0;dd!=3;dd++) { //normalize the vector
          //   ray_dir[dd] /= norm;
          // }

          std::vector< EntityHandle > triangles_out;
          vector<double> dist;
          rval = this->kd_tree->ray_intersect_triangles(this->kdTreeRootMeshset,tol,ray_dir,coords,triangles_out,dist);
          CHKERRQ_MOAB(rval);

          if(triangles_out.size()%2 == 0 ){
          //point does not belong to the volume - erase
            it = influenceIndicesData.erase(it);
          // if(it==influenceIndicesData.end()) {
          //     break;
          //   }
          } else {
            // EntityHandle new_vertex = 0;
            // rval = moab_testing->create_vertex(&coords[0],new_vertex); CHKERRQ_MOAB(rval);
            ++it;
          }
        }
      // }
      }
    int influence_nodes_size = influenceIndicesData.size();
    A.resize(nbBasePolynomials,nbBasePolynomials,false);
    B.resize(nbBasePolynomials,influence_nodes_size,false);
    A.clear();
    B.clear();
    for(int d = 0;d!=3;d++) {
      diffA[d].resize(nbBasePolynomials,nbBasePolynomials,false);
      diffB[d].resize(nbBasePolynomials,influence_nodes_size,false);
      diffA[d].clear();
      diffB[d].clear();
    }

    // Local base functions
    baseFun.resize(nbBasePolynomials,false);
    baseFun.clear();
    baseFun[0] = 1;
    for(int d = 0;d!=3;d++) {
      diffBaseFun[d].resize(nbBasePolynomials,false);
      diffBaseFun[d].clear();
    }

    int ii = 0;
    for(list<VoxelData>::iterator it = influenceIndicesData.begin();
    it != influenceIndicesData.end();++it, ++ii) {

    double node_coords[3] = {(*it).cOords[0],(*it).cOords[1],(*it).cOords[2]};
    FTensor::Tensor1<double*,3> t_node_coords(node_coords,&node_coords[1],&node_coords[2]);
    t_node_coords(i) -= t_material_coords(i);
    t_node_coords(i) /= dm;
    const double r = sqrt(t_node_coords(i)*t_node_coords(i));

    // const double r = *it / dm;
    //weights
    const double w = evalWeight(r);
    const double diff_w_r = evalDiffWeight(r)/dm;

    if(nbBasePolynomials>1) {
      baseFun[1] = t_node_coords(0);
      baseFun[2] = t_node_coords(1);
      baseFun[3] = t_node_coords(2);
    }
    if(nbBasePolynomials>4) {
      baseFun[4] = t_node_coords(0)*t_node_coords(0);
      baseFun[5] = t_node_coords(1)*t_node_coords(1);
      baseFun[6] = t_node_coords(2)*t_node_coords(2);
      baseFun[7] = t_node_coords(0)*t_node_coords(1);
      baseFun[8] = t_node_coords(0)*t_node_coords(2);
      baseFun[9] = t_node_coords(1)*t_node_coords(2);
    }
    for(int k = 0;k!=nbBasePolynomials;k++) {
      const double wp = w*baseFun[k];
      for(int j = 0;j<=k;j++) {
        A(k,j) += wp*baseFun[j];
      }
      B(k,ii) = wp;
      const double diff_wp_r = diff_w_r*baseFun[k];
      for(int d = 0;d!=3;d++) {
        const double diff_wp = diff_wp_r*copysign(1.,t_node_coords(d));
        for(int j = 0;j<=k;j++) {
          diffA[d](k,j) += diff_wp*baseFun[j];
        }
        diffB[d](k,ii) = diff_wp;
      }
    }
  }
  invA.resize(nbBasePolynomials,nbBasePolynomials,false);
  approxFun.resize(influence_nodes_size,false);
  for(int d = 0;d!=3;d++) {
    diffInvA[d].resize(nbBasePolynomials,nbBasePolynomials,false);
    diffApproxFun.resize(3,influence_nodes_size,false);
  }

  // Invert martrix
  {
    L.resize(nbBasePolynomials,nbBasePolynomials,false);
    if(cholesky_decompose(A,L)) {
      cerr << A << endl;
      SETERRQ(
        mField.get_comm(),
        MOFEM_OPERATION_UNSUCCESSFUL,
        "Failed to invert matrix - solution could be "
        "to increase dmFactor to bigger value, e.g. -mwls_dm 4, "
        "or reduce of number of base fuctions -mwls_number_of_base_functions 1"
      );
    }
    invA.clear();
    for(int k = 0;k!=nbBasePolynomials;k++) {
      ublas::matrix_column<MatrixDouble > mr(invA,k);
      mr(k) = 1;
      cholesky_solve(L,mr,ublas::lower());
    }
  }

  for(int d = 0;d!=3;d++) {
    MatrixDouble a = prod(diffA[d],invA);
    noalias(diffInvA[d]) = -prod(invA,a);
  }

  baseFun[0] = 1;
  for(int d = 0;d!=3;d++) {
    diffBaseFun[d][0] = 0;
  }
  if(nbBasePolynomials>1) {
    baseFun[1] = 0;//material_coords[0];
    baseFun[2] = 0;//material_coords[1];
    baseFun[3] = 0;//material_coords[2];
    // DX
    diffBaseFun[0][1] = 1./dm;
    diffBaseFun[0][2] = 0;
    diffBaseFun[0][3] = 0;
    // DY
    diffBaseFun[1][1] = 0;
    diffBaseFun[1][2] = 1./dm;
    diffBaseFun[1][3] = 0;
    // DZ
    diffBaseFun[2][1] = 0;
    diffBaseFun[2][2] = 0;
    diffBaseFun[2][3] = 1./dm;
  }

  if(nbBasePolynomials>4) {
    baseFun[4] = 0;//material_coords[0]*material_coords[0];
    baseFun[5] = 0;//material_coords[1]*material_coords[1];
    baseFun[6] = 0;//material_coords[2]*material_coords[2];
    baseFun[7] = 0;//material_coords[0]*material_coords[1];
    baseFun[8] = 0;//material_coords[0]*material_coords[2];
    baseFun[9] = 0;//material_coords[1]*material_coords[2];
    // DX
    diffBaseFun[0][4] = 0;//2*material_coords[0];
    diffBaseFun[0][5] = 0;//0;
    diffBaseFun[0][6] = 0;//0;
    diffBaseFun[0][7] = 0;//material_coords[1];
    diffBaseFun[0][8] = 0;//material_coords[2];
    diffBaseFun[0][9] = 0;//0;
    // DY
    diffBaseFun[1][4] = 0;//0;
    diffBaseFun[1][5] = 0;//2*material_coords[1];
    diffBaseFun[1][6] = 0;//0;
    diffBaseFun[1][7] = 0;//material_coords[0];
    diffBaseFun[1][8] = 0;//0;
    diffBaseFun[1][9] = 0;//material_coords[2];
    // DZ
    diffBaseFun[2][4] = 0;//0;
    diffBaseFun[2][5] = 0;//0;
    diffBaseFun[2][6] = 0;//2*material_coords[2];
    diffBaseFun[2][7] = 0;//0;
    diffBaseFun[2][8] = 0;//material_coords[0];
    diffBaseFun[2][9] = 0;//material_coords[1];
  }

  invAB.resize(nbBasePolynomials,influence_nodes_size,false);
  noalias(invAB) = prod(invA,B);
  VectorDouble baseFunInvA = prod(baseFun,invA);

  // Calculate approximation functions
    noalias(approxFun) = prod(baseFun,invAB);
  // cerr << approxFun << endl;

  // Calculate derivative of base functions
  VectorDouble tmp;
  tmp.resize(nbBasePolynomials,false);
    for(int d = 0;d!=3;d++) {
      ublas::matrix_row<MatrixDouble > mr(diffApproxFun,d);
      noalias(mr) = prod(diffBaseFun[d],invAB);
      noalias(tmp) = prod(baseFun,diffInvA[d]);
      mr += prod(tmp,B);
      mr += prod(baseFunInvA,diffB[d]);
    }


  PetscFunctionReturn(0);
}
 PetscErrorCode MovingLeastSquares::approxMHDData() {
  PetscFunctionBegin;

  int size = influenceIndicesData.size();
  influenceNodesData.resize(size,1,false);
  int ii = 0;
  for(list<VoxelData>::iterator it = influenceIndicesData.begin();
  it != influenceIndicesData.end();++it,++ii) {
    influenceNodesData(ii,0) = it->dAta;
  }
  outData.resize(1,false);
  outDiffData.resize(3,1,false);
  PetscFunctionReturn(0);

 }
#endif //WITH METAIO

 PetscErrorCode MovingLeastSquares::calculateApproxFun(
   double material_coords[3],
   bool calc_base,
   bool calc_diff_base,
   bool trim_influence_nodes
 ) {
   MoFEMFunctionBeginHot;

   FTensor::Tensor1<double,3> t_material_coords(
     material_coords[0],material_coords[1],material_coords[2]
   );

   // Node coordinates
   double node_coords[3];
   FTensor::Tensor1<double*,3> t_node_coords(node_coords,&node_coords[1],&node_coords[2]);

   // Determine points in influence volume
   const double dm = maxEdgeL*dmFactor; // influence radius
   Range& influence_nodes = influenceNodes;
   if(trim_influence_nodes) {
    //  int ii = 0;
     int jj = 0;
     for(Range::iterator nit = influence_nodes.begin();nit!=influence_nodes.end();jj++) {
       rval = mwlsMoab.get_coords(&*nit,1,node_coords); CHKERRQ_MOAB(rval);
       t_node_coords(i) -= t_material_coords(i);
       const double r = sqrt(t_node_coords(i)*t_node_coords(i))/dm;
       if(r>1) {
         nit = influence_nodes.erase(nit);
         if(nit==influence_nodes.end()) {
           break;
         }
       } else {
         nit++;
       }
     }
   }


   A.resize(nbBasePolynomials,nbBasePolynomials,false);
   B.resize(nbBasePolynomials,influence_nodes.size(),false);
   A.clear();
   B.clear();
   for(int d = 0;d!=3;d++) {
     diffA[d].resize(nbBasePolynomials,nbBasePolynomials,false);
     diffB[d].resize(nbBasePolynomials,influence_nodes.size(),false);
     diffA[d].clear();
     diffB[d].clear();
   }

   // Local base functions
   baseFun.resize(nbBasePolynomials,false);
   baseFun.clear();
   baseFun[0] = 1;
   for(int d = 0;d!=3;d++) {
     diffBaseFun[d].resize(nbBasePolynomials,false);
     diffBaseFun[d].clear();
   }

   {
     int ii = 0;
     for(Range::iterator nit = influence_nodes.begin();nit!=influence_nodes.end();nit++,ii++) {
       rval = mwlsMoab.get_coords(&*nit,1,node_coords); CHKERRQ_MOAB(rval);
       t_node_coords(i) -= t_material_coords(i);
       t_node_coords(i) /= dm;
       const double r = sqrt(t_node_coords(i)*t_node_coords(i));
       // Weights
       const double w = evalWeight(r);
       const double diff_w_r = evalDiffWeight(r)/dm;
       if(nbBasePolynomials>1) {
         baseFun[1] = t_node_coords(0);
         baseFun[2] = t_node_coords(1);
         baseFun[3] = t_node_coords(2);
       }
       if(nbBasePolynomials>4) {
         baseFun[4] = t_node_coords(0)*t_node_coords(0);
         baseFun[5] = t_node_coords(1)*t_node_coords(1);
         baseFun[6] = t_node_coords(2)*t_node_coords(2);
         baseFun[7] = t_node_coords(0)*t_node_coords(1);
         baseFun[8] = t_node_coords(0)*t_node_coords(2);
         baseFun[9] = t_node_coords(1)*t_node_coords(2);
       }
       for(int k = 0;k!=nbBasePolynomials;k++) {
         const double wp = w*baseFun[k];
         for(int j = 0;j<=k;j++) {
           A(k,j) += wp*baseFun[j];
         }
         B(k,ii) = wp;
         const double diff_wp_r = diff_w_r*baseFun[k];
         for(int d = 0;d!=3;d++) {
           const double diff_wp = diff_wp_r*copysign(1.,t_node_coords(d));
           for(int j = 0;j<=k;j++) {
             diffA[d](k,j) += diff_wp*baseFun[j];
           }
           diffB[d](k,ii) = diff_wp;
         }
       }
     }
   }

   invA.resize(nbBasePolynomials,nbBasePolynomials,false);
   approxFun.resize(influence_nodes.size(),false);
   for(int d = 0;d!=3;d++) {
     diffInvA[d].resize(nbBasePolynomials,nbBasePolynomials,false);
     diffApproxFun.resize(3,influence_nodes.size(),false);
   }

   // Invert matrix
   {
     L.resize(nbBasePolynomials,nbBasePolynomials,false);
     if(cholesky_decompose(A,L)) {
       cerr << A << endl;
       SETERRQ(
         mField.get_comm(),
         MOFEM_OPERATION_UNSUCCESSFUL,
         "Failed to invert matrix - solution could be "
         "to increase dmFactor to bigger value, e.g. -mwls_dm 4, "
         "or reduce of number of base fuctions -mwls_number_of_base_functions 1"
       );
     }
     invA.clear();
     for(int k = 0;k!=nbBasePolynomials;k++) {
       ublas::matrix_column<MatrixDouble > mr(invA,k);
       mr(k) = 1;
       cholesky_solve(L,mr,ublas::lower());
     }
   }

   for(int d = 0;d!=3;d++) {
     MatrixDouble a = prod(diffA[d],invA);
     noalias(diffInvA[d]) = -prod(invA,a);
   }

   baseFun[0] = 1;
   for(int d = 0;d!=3;d++) {
     diffBaseFun[d][0] = 0;
   }
   if(nbBasePolynomials>1) {
     baseFun[1] = 0;//material_coords[0];
     baseFun[2] = 0;//material_coords[1];
     baseFun[3] = 0;//material_coords[2];
     // DX
     diffBaseFun[0][1] = 1./dm;
     diffBaseFun[0][2] = 0;
     diffBaseFun[0][3] = 0;
     // DY
     diffBaseFun[1][1] = 0;
     diffBaseFun[1][2] = 1./dm;
     diffBaseFun[1][3] = 0;
     // DZ
     diffBaseFun[2][1] = 0;
     diffBaseFun[2][2] = 0;
     diffBaseFun[2][3] = 1./dm;
   }

   if(nbBasePolynomials>4) {
     baseFun[4] = 0;//material_coords[0]*material_coords[0];
     baseFun[5] = 0;//material_coords[1]*material_coords[1];
     baseFun[6] = 0;//material_coords[2]*material_coords[2];
     baseFun[7] = 0;//material_coords[0]*material_coords[1];
     baseFun[8] = 0;//material_coords[0]*material_coords[2];
     baseFun[9] = 0;//material_coords[1]*material_coords[2];
     // DX
     diffBaseFun[0][4] = 0;//2*material_coords[0];
     diffBaseFun[0][5] = 0;//0;
     diffBaseFun[0][6] = 0;//0;
     diffBaseFun[0][7] = 0;//material_coords[1];
     diffBaseFun[0][8] = 0;//material_coords[2];
     diffBaseFun[0][9] = 0;//0;
     // DY
     diffBaseFun[1][4] = 0;//0;
     diffBaseFun[1][5] = 0;//2*material_coords[1];
     diffBaseFun[1][6] = 0;//0;
     diffBaseFun[1][7] = 0;//material_coords[0];
     diffBaseFun[1][8] = 0;//0;
     diffBaseFun[1][9] = 0;//material_coords[2];
     // DZ
     diffBaseFun[2][4] = 0;//0;
     diffBaseFun[2][5] = 0;//0;
     diffBaseFun[2][6] = 0;//2*material_coords[2];
     diffBaseFun[2][7] = 0;//0;
     diffBaseFun[2][8] = 0;//material_coords[0];
     diffBaseFun[2][9] = 0;//material_coords[1];
   }

   invAB.resize(nbBasePolynomials,influence_nodes.size(),false);
   noalias(invAB) = prod(invA,B);
   VectorDouble baseFunInvA = prod(baseFun,invA);

   // Calulate approximation functions
   if(calc_base) {
     noalias(approxFun) = prod(baseFun,invAB);
   }
   // cerr << approxFun << endl;

   // Caluclate direvative of base functions
   VectorDouble tmp;
   tmp.resize(nbBasePolynomials,false);
   if(calc_diff_base) {
     for(int d = 0;d!=3;d++) {
       ublas::matrix_row<MatrixDouble > mr(diffApproxFun,d);
       noalias(mr) = prod(diffBaseFun[d],invAB);
       noalias(tmp) = prod(baseFun,diffInvA[d]);
       mr += prod(tmp,B);
       mr += prod(baseFunInvA,diffB[d]);
     }
   }

   MoFEMFunctionReturnHot(0);
 }


 PetscErrorCode MovingLeastSquares::approxTagData(Tag th) {
   MoFEMFunctionBeginHot;
   int length;
   rval = mwlsMoab.tag_get_length(th,length); CHKERRQ_MOAB(rval);
   influenceNodesData.resize(influenceNodes.size(),length,false);
   rval = mwlsMoab.tag_get_data(th,influenceNodes,&influenceNodesData(0,0));
   outData.resize(length,false);
   outDiffData.resize(3,length,false);
   MoFEMFunctionReturnHot(0);
 }

 MovingLeastSquares::VecVals& MovingLeastSquares::getData() {
   noalias(outData) = prod(approxFun,influenceNodesData);
   return outData;
 }

 MovingLeastSquares::MatDiffVals& MovingLeastSquares::getDiffData() {
   noalias(outDiffData) = prod(diffApproxFun,influenceNodesData);
   return outDiffData;
 }


}
