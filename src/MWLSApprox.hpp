/** \file MWLSApprox.hpp
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __MWLSAPPROX_HPP__
#define __MWLSAPPROX_HPP__

namespace MWLSApproxSpace {

  /**

  Moving weighted least square approximation (MWLS)

  \f[
  u^h(x) = p_k(x)a_k(x)\\
  J(x)=\frac{1}{2} \sum_i w(x-x_i)\left(p_j(x_i)a_j(x)-u_i\right)^2 = \\
  J(x)=\frac{1}{2} \sum_i w(x-x_i)\left( p_j(x_i)a_j(x) p_k(x_i)a_k(x) - 2p_j(x_i)a_j(x)u_i + u_i^2 \right) \\
  \frac{\partial J}{\partial a_j} =
  \sum_i w(x_i-x)\left(p_j(x_i)p_k(x_i) a_k(x) -p_j(x_j)u_i\right)\\
  A_{jk}(x) = \sum_i w(x_i-x)p_j(x_i)p_k(x_i)\\
  B_{ji}(x) = w(x-x_i) p_j(x_i)\\
  A_{jk}(x) a_k(x) = B_{ji}(x)u_i\\
  a_k(x) = \left( A^{-1}_{kj}(x) B_{ji}(x) \right) u_i\\
  u^h(x) = p_k(x) \left( A^{-1}_{kj}(x) B_{ji}(x) \right) u_i \\
  u^h(x) = \left[ p_k(x) \left( A^{-1}_{kj}(x) B_{ji}(x) \right) \right] u_i \\
  \phi_i = p_k(x) \left( A^{-1}_{kj}(x) B_{ji}(x) \right) \\
  \phi_{i,l} =
  p_{k,l}(x) \left( A^{-1}_{kj}(x) B_{ji}(x) \right)+
  p_k(x) \left( A^{-1}_{kj,l}(x) B_{ji}(x) + A^{-1}_{kj}(x) B_{ji,l}(x)  \right)\\
  \phi_{i,l} =
  p_{k,l}(x) \left( A^{-1}_{kj}(x) B_{ji}(x) \right)+
  p_k(x) \left( A^{-1}_{kj,l}(x) B_{ji}(x) + A^{-1}_{kj}(x) B_{ji,l}(x)  \right)\\
  \left(\mathbf{A}\mathbf{A}^{-1}\right)_{,l} = \mathbf{0} \to \mathbf{A}^{-1}_{,l} = -\mathbf{A}^{-1} \mathbf{A}_{,l} \mathbf{A}^{-1}\\
  A_{jk,l} = \sum_i w_{,l}(x_i-x)p_j(x_i)p_k(x_i) \\
  B_{ij,l} = w_{,l}(x_i-x)p_j(x_i)
  \f]

  */
  struct MovingLeastSquares {

    MoFEM::Interface &mField;
    moab::Core mwlsCore;
    moab::Interface& mwlsMoab;


    MovingLeastSquares(
      MoFEM::Interface &m_field
    ):
    mField(m_field),
    mwlsMoab(mwlsCore),
    nbBasePolynomials(1),
    dmFactor(1) {
    }

    ~MovingLeastSquares() {
      // if(myTree) {
      //   myTree->reset_tree();
      // }
    }
    
    #ifdef WITH_METAIO

    Range sKin;
    boost::shared_ptr<AdaptiveKDTree> kd_tree;
    EntityHandle kdTreeRootMeshset; 
    PetscBool trimInfluenceNodes;
    PetscBool mhdRhoFile;

    double sCaleMHD;
    double tHresholdMHD;
    double paramDensity[2];
    //moab_testing
    // moab::Core mb_testing;
    // moab::Interface *moab_testing;

    struct VoxelData {
      VectorDouble3 cOords;
      double dAta;
      VoxelData(
        const VectorDouble3& coords,const double data
      ): cOords(coords),dAta(data) {}
      VoxelData(
        const double x,const double y, const double z,const double data
      ): cOords(3),dAta(data) {
        cOords[0] = x;
        cOords[1] = y;
        cOords[2] = z;
      }
      
    };

    boost::shared_ptr<MetaImage> metaFile;
    list<VoxelData> influenceIndicesData;
  
    #endif  // WITH_METAIO
      
    /**
    * \brief get options from command line for MWLS
    * @return error code
    */
    PetscErrorCode getMWLSOptions();

    /**
    * Load mesh with data and do basic calculation
    * @param  file_name File name
    * @param  med_flg flag for med file
    * @return           Error code
    */
    PetscErrorCode loadMWLSMesh(std::string file_name,bool med_flg = true);

    /**
     * \brief get values form elements to nodes
     * @param  th tag with approximated data
     * @return    error code
     */
    PetscErrorCode getValuesToNodes(Tag th);
    
    /**
     * \brief Get noded in influence domain
     * @param  material_coords material position
     * @return error code
     */
    PetscErrorCode getInfluenceNodes(double material_coords[3],bool search_point = true);
    
    #ifdef WITH_METAIO
    /**
    * Load mhd file with data and do basic calculations
    * @param  file_name File name
    * @return           Error code
    */
    PetscErrorCode loadMHData(std::string file_name);
    
    /**
    * \brief Get nodes in influence domain
    * from meta file
    * @param  material_coords material position
    * @return error code
    */
    PetscErrorCode getInfluenceNodesFromMeta(double material_coords[3]);

  /**
   * \brief  calculate approximation function at material point
   * 
   * @param material_coords material position 
   * @return PetscErrorCode error code
   */
    PetscErrorCode calculateApproxFunMHD(
      double material_coords[3]
    );

    /**
    * Get tag data on influence nodes
    * @return    error code
    */
    PetscErrorCode approxMHDData();

    #endif // WITH_METAIO
    
    /**
    * \brief Calculate approximation function at material point
    * @param  material_coords material position
    * @return          error code

    NOTE: This function remove form influenceNodes entities which
    are beyond influence radius.

    */
    PetscErrorCode calculateApproxFun(
      double material_coords[3],
      bool calc_base = true,
      bool calc_diff_base = true,
      bool trim_influence_nodes = true
    );

    /**
    * Get tag data on influence nodes
    * @param  th tag
    * @return    error code
    */
    PetscErrorCode approxTagData(Tag th);

    typedef ublas::vector<double,ublas::bounded_array<double,9> > VecVals;
    typedef ublas::matrix<double,ublas::row_major,ublas::bounded_array<double,27> > MatDiffVals;

    /**
    * Get values at point
    * @return error code
    */
    VecVals& getData();

    /**
    * Get direvatives of values at points
    * @return error code
    */
    MatDiffVals& getDiffData();


  private:

    boost::shared_ptr<AdaptiveKDTree> myTree;
    boost::shared_ptr<ElemEvaluator> elemEval;

    // 1 (1) x y z (4) xx yy zz xy xz yz (10)
    int nbBasePolynomials;  ///< Number of base functions (1 - linear, 4 - linear, 10 - quadratic)
    double dmFactor;        ///< Controls influence radius r = dmFactor*edge_length

    EntityHandle treeRoot;
    double maxEdgeL;

    Range levelNodes,levelEdges,levelTets;
    Range influenceNodes;

    ublas::symmetric_matrix<double,ublas::lower> A;
    MatrixDouble invA;
    MatrixDouble invAB;
    ublas::triangular_matrix<double,ublas::lower> L;
    MatrixDouble B;
    VectorDouble baseFun;
    VectorDouble approxFun;

    ublas::symmetric_matrix<double,ublas::lower> diffA[3];
    ublas::symmetric_matrix<double,ublas::lower> diffInvA[3];
    MatrixDouble diffB[3];
    VectorDouble diffBaseFun[3];
    MatrixDouble diffApproxFun;

    MatrixDouble influenceNodesData;
    VecVals outData;
    MatDiffVals outDiffData;

    FTensor::Index<'i',3> i;

    inline double evalWeight(const double r) {
      const double rr = r*r;
      const double rrr = rr*r;
      const double r4 = rrr*r;
      if(r<1) {
        return 1-6*rr+8*rrr-3*r4;
      } else {
        return 0;
      }
      // if(r<=0.5) {
      //   return 2./3-4*rr+4*rrr;
      // } else if(r<=1) {
      //   return 4./3-4*r+4*rr-(4./3)*rrr;
      // } else return 0;
    }

    inline double evalDiffWeight(const double r) {
      const double rr = r*r;
      const double rrr = rr*r;
      if(r<1) {
        return -12*r+24*rr-12*rrr;
      } else {
        return 0;
      }
      // if(r<=0.5) {
      //   return -8*r+12*rr;
      // } else if(r<=1) {
      //   return -4+8*r-4*rr;
      // } else return 0;
    }

    double boxSize;
    PetscErrorCode getBox(
      double material_coords[3],
      std::vector<double>& coords
    );

  };

}

#endif //__MWLSAPPROX_HPP__
