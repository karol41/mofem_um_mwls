/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifdef WITH_TETGEN
  #include <tetgen.h>
#endif
#ifdef REAL
  #undef REAL
#endif

#ifdef WITH_METAIO
  #include <metaImage.h>
#endif

// #include <moab/BVHTree.hpp>
#include <MoFEM.hpp>
#include <moab/SpatialLocator.hpp>
using namespace MoFEM;
#include <MWLSApprox.hpp>

#include <moab/AdaptiveKDTree.hpp>
#include <moab/Skinner.hpp>

static char help[] = "Testing moving weighted least approximation"
                     "n\n";

using namespace MWLSApproxSpace;

int main(int argc, char *argv[]) {
  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);

  try {

    PetscBool flg = PETSC_TRUE;
    char mesh_file_name[255];
    ierr = PetscOptionsGetString(PETSC_NULL, "", "-my_file", mesh_file_name,
                                 255, &flg);
    CHKERRQ(ierr);

    if (flg != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    const char *option;
    option = "";
    // CHKERR moab.load_file(mesh_file_name, 0, option);
    rval   = moab.load_file(mesh_file_name, 0, option);
    CHKERRQ_MOAB(rval);
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    std::string mwlsApproxFile;    ///< Name of file with internal stresses
    std::string mwlsStressTagName; ///< Name of tag with internal stresses
    int residualStressBlock = -1; ///< Block on which residual stress are applied
    std::string medFileName;
    std::string configFile;
    PetscBool flg_med;

    ierr = PetscOptionsBegin(m_field.get_comm(), "", "MWLS module", "none");
    CHKERRQ(ierr);

    PetscBool flg;
    char file_name[255] = "mwls.med";
    CHKERR PetscOptionsString(
        "-my_mwls_approx_file",
        "file with data form med file (code-aster) with radiation data", "",
        file_name, file_name, 255, &flg);
    if (flg == PETSC_TRUE) {
      mwlsApproxFile = std::string(file_name);
    }
    char tag_name[255] = "SIGP";
    CHKERR PetscOptionsString("-my_internal_stress_name",
                              "name of internal stress tag", "", tag_name,
                              tag_name, 255, &flg);
    mwlsStressTagName = "MED_" + std::string(tag_name);

    CHKERR PetscOptionsInt("-my_residual_stress_block",
                           "block to which residual stress is applied", "",
                           residualStressBlock, &residualStressBlock,
                           PETSC_NULL);
    
    // char file_name[255] = "file.med";
    CHKERR PetscOptionsString("-my_med_file", "read med file", "", file_name,
                              file_name, 255, &flg_med);
    if (flg_med) {
      medFileName = std::string(file_name);
    }
    
    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    MeshsetsManager *block_manager_ptr;
    CHKERR m_field.getInterface(block_manager_ptr);
    EntityHandle approx_meshset = 0;
    if (residualStressBlock != -1) {
      CHKERR block_manager_ptr->getMeshset(residualStressBlock, BLOCKSET,
                                           approx_meshset);
    }
    // get tests on which stress field is going to be approximated
    Range tets;
    CHKERR moab.get_entities_by_type(approx_meshset, MBTET, tets, true);
    // cerr << tets << endl;
    // Range edges;
    // rval = moab.get_adjacencies(tets,1,true,edges,moab::Interface::UNION);
    // CHKERRQ_MOAB(rval);
    EntityHandle meshset;
    CHKERR moab.create_meshset(MESHSET_SET, meshset);
    CHKERR moab.add_entities(meshset, tets);

    //  CHKERR  moab.convert_entities(meshset,true,false,false);
    // CHKERRQ_MOAB(rval);

    // create instance of mwls class
    MovingLeastSquares mwls(m_field);

    bool check_file_ext =
        mwlsApproxFile.substr(mwlsApproxFile.length() - 3) == "mhd";
    if (check_file_ext) {

      // TODO error if file has extension mhd and METAIO is not installed
      #ifndef WITH_METAIO
          SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_FOUND,"You need to compile users modules with MetaIO -DWITH_METAIO=1");
        // #error "You need compile users modules with MetaIO -DWITH_METAIO=1"
      #endif

      #ifdef WITH_METAIO
      ierr = mwls.loadMHData(mwlsApproxFile.c_str());
      Tag th_approx_rho;
      Tag tag_approx_handles_diff;
      std::string approx_name      = "RHO_APPROX";
      std::string approx_diff_name = "APPROX_DRHO";
      double def_vals[3];
      fill(def_vals, &def_vals[3], 0);
      CHKERR moab.tag_get_handle(approx_name.c_str(), 1, MB_TYPE_DOUBLE,
                                 th_approx_rho, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);
      // add tags to the lists
      Tag th_approx_diff;
      CHKERR moab.tag_get_handle(approx_diff_name.c_str(), 3, MB_TYPE_DOUBLE,
                                 th_approx_diff, MB_TAG_CREAT | MB_TAG_SPARSE,
                                 def_vals);
      tag_approx_handles_diff = th_approx_diff;

      // take a skin
      Skinner skin(&moab);
      CHKERR skin.find_skin(0, tets, false, mwls.sKin);
      mwls.kd_tree =
          boost::shared_ptr<AdaptiveKDTree>(new AdaptiveKDTree(&moab));
      CHKERR mwls.kd_tree->build_tree(mwls.sKin, &mwls.kdTreeRootMeshset);

      // get nodes from the meshset to approximate data on
      Range nodes;
      CHKERR moab.get_connectivity(tets, nodes, false);
      // loop over all nodes
      PetscPrintf(PETSC_COMM_WORLD,
                  "Pre-processing MHD data from < %d > points \n",
                  nodes.size());
      int bar = 0;
      for (Range::iterator nit = nodes.begin(); nit != nodes.end(); ++nit) {
        double coords[3];
        CHKERR moab.get_coords(&*nit, 1, coords);
        // find nodes in influence radius on med mesh
        CHKERR mwls.getInfluenceNodesFromMeta(coords);

        CHKERR mwls.calculateApproxFunMHD(coords);

        CHKERR mwls.approxMHDData();

        MovingLeastSquares::VecVals vals = mwls.getData();
        // get direvatives
        MovingLeastSquares::MatDiffVals diff_vals = mwls.getDiffData();
        // cerr << diff_vals << endl;
        CHKERR moab.tag_set_data(th_approx_rho, &*nit, 1,
                                 &*vals.data().begin());
        CHKERR moab.tag_set_data(tag_approx_handles_diff, &*nit, 1,
                                 &diff_vals(0, 0));
        // bar to show the progress
        int inc = nodes.size() / 10 ? nodes.size() / 10 : 1;
        if ((bar++) % inc == 0)
          PetscPrintf(PETSC_COMM_WORLD, " . ");
      }
      // moab_testing
      // cout << "\nNow write debug mesh ...\n";
      // rval = mwls.moab_testing->write_file("test_out.vtk","VTK","");
      // CHKERRQ_MOAB(rval);

      PetscPrintf(PETSC_COMM_WORLD, "\nDone. Saving files... \n");
#endif

    } else {
      // load mesh with the stresses
      CHKERR mwls.loadMWLSMesh(mwlsApproxFile.c_str(),flg_med);

      // Create tags to approximate data
      std::vector<Tag> tag_handles;
      // Get handles for all tags defined in the mesh instance.
      CHKERR mwls.mwlsMoab.tag_get_tags(tag_handles);
      ublas::vector<Tag> tag_approx_handles(tag_handles.size());
      // cout << "tag handles size is " << tag_handles.size() << endl;
      ublas::matrix<Tag> tag_approx_handles_diff(3, tag_handles.size());
      double def_vals[9];
      fill(def_vals, &def_vals[9], 0);
      // interate over all tags
      for (int t = 0; t != tag_handles.size(); t++) {
        // get tag name
        std::string name;
        CHKERR mwls.mwlsMoab.tag_get_name(tag_handles[t], name);
        // cerr << name << endl;
        // check if tag is med tag
        if (name.compare(0, 3, "MED") == 0) {
          PetscPrintf(PETSC_COMM_WORLD, "Pre-process tag data < %s >\n",
                      name.c_str());
          // get tag values at nodes
          CHKERR mwls.getValuesToNodes(tag_handles[t]);
          // create tags on computational mesh for approximated values and
          // derivatives of values
          std::string approx_name        = "APPROX_" + name;
          // std::string approx_name        = "RHO_APPROX" + name;
          std::string approx_diff_name[] = {
              "APPROX_DX_" + name, "APPROX_DY_" + name, "APPROX_DZ_" + name};
          // get length and type of med tag on med mesh
          int length;
          CHKERR mwls.mwlsMoab.tag_get_length(tag_handles[t], length);
          DataType data_type;
          CHKERR mwls.mwlsMoab.tag_get_data_type(tag_handles[t], data_type);
          // create tag on computational mesh
          Tag th_approx;
          CHKERR moab.tag_get_handle(approx_name.c_str(), length, data_type,
                                     th_approx, MB_TAG_CREAT | MB_TAG_SPARSE,
                                     def_vals);
          // add tags to the lists
          tag_approx_handles[t] = th_approx;
          for (int d = 0; d != 3; d++) {
            Tag th_approx_diff;
            CHKERR moab.tag_get_handle(approx_diff_name[d].c_str(), length,
                                       data_type, th_approx_diff,
                                       MB_TAG_CREAT | MB_TAG_SPARSE, def_vals);
            tag_approx_handles_diff(d, t) = th_approx_diff;
          }
        }
        if (name.compare(0, 3, "RHO") == 0 || name.compare("rho") == 0) {
          PetscPrintf(PETSC_COMM_WORLD, "Pre-process tag data < %s >\n",
                      name.c_str());
          // get tag values at nodes
          // CHKERR mwls.getValuesToNodes(tag_handles[t]);
          // create tags on computational mesh for approximated values and
          // derivatives of values
          std::string approx_name        = "RHO";
          std::string approx_diff_name[] = {
              "APPROX_DX_" + name, "APPROX_DY_" + name, "APPROX_DZ_" + name};
          // get length and type of med tag on med mesh
            // Range nodes_tets;
            // CHKERR mwls.mwlsMoab.get_entities_by_type(0, MBVERTEX, nodes_tets, true);
            // for ( auto it : nodes_tets){
            //   double var;
              
            //   mwls.mwlsMoab.tag_get_data(tag_handles[t],&it,1,&var);
            //   cerr << "DATA on " << it << " is: " << var << '\n';
            // }

          int length;
          CHKERR mwls.mwlsMoab.tag_get_length(tag_handles[t], length);
          DataType data_type;
          CHKERR mwls.mwlsMoab.tag_get_data_type(tag_handles[t], data_type);
          // create tag on computational mesh
          Tag th_approx;
          CHKERR moab.tag_get_handle(approx_name.c_str(), length, data_type,
                                     th_approx, MB_TAG_CREAT | MB_TAG_SPARSE,
                                     def_vals);
          tag_approx_handles[t] = th_approx;
          // add tags to the lists
          for (int d = 0; d != 3; d++) {
            Tag th_approx_diff;
            CHKERR moab.tag_get_handle(approx_diff_name[d].c_str(), length,
                                       data_type, th_approx_diff,
                                       MB_TAG_CREAT | MB_TAG_SPARSE, def_vals);
            tag_approx_handles_diff(d, t) = th_approx_diff;
          }
        }
        
      }

      // get nodes on tets on computational mesh
      Range nodes;
      CHKERR moab.get_connectivity(tets, nodes, false);
      // cerr << nodes << endl;

      // Approximate tags
      for (Range::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
        // get coords
        double coords[3];
        CHKERR moab.get_coords(&*nit, 1, coords);
        // find nodes in influence radius on med mesh
        CHKERR mwls.getInfluenceNodes(coords);
        // calculate approximation/base functions
        CHKERR mwls.calculateApproxFun(coords);
        // loop over all tags and use mwls to approximate med tags on
        // computational nodes results save on the compositional mesh tags
        for (int t = 0; t != tag_handles.size(); t++) {
          std::string name;
          CHKERR mwls.mwlsMoab.tag_get_name(tag_handles[t], name);
          if (name.compare(0, 3, "MED") == 0) {
            if (nit == nodes.begin()) {
              PetscPrintf(PETSC_COMM_WORLD, "Approximate tag < %s >\n",
                          name.c_str());
            }
            // approximate data
            CHKERR mwls.approxTagData(tag_handles[t]);
            // get values
            MovingLeastSquares::VecVals vals = mwls.getData();
            // get direvatives
            MovingLeastSquares::MatDiffVals diff_vals = mwls.getDiffData();
            // cerr << diff_vals << endl;
            // save values on tags
            CHKERR moab.tag_set_data(tag_approx_handles[t], &*nit, 1,
                                     &*vals.data().begin());
            // save direvatives on tags
            for (int d = 0; d != 3; d++) {
              CHKERR moab.tag_set_data(tag_approx_handles_diff(d, t), &*nit, 1,
                                       &diff_vals(d, 0));
            }
          }
          if (name.compare(0, 3, "RHO") == 0 || name.compare("rho") == 0  )  {
            if (nit == nodes.begin()) {
              PetscPrintf(PETSC_COMM_WORLD, "Approximate tag < %s >\n",
                          name.c_str());
            }
            // double valus[3];
            // mwls.mwlsMoab.tag_get_data(tag_handles[t],&*nit,1,valus); CHKERRQ_MOAB(rval);
            // cerr << "data: " << valus[0] << "\n";
            // approximate data
            CHKERR mwls.approxTagData(tag_handles[t]);
            // get values
            MovingLeastSquares::VecVals vals = mwls.getData();
            // get direvatives
            MovingLeastSquares::MatDiffVals diff_vals = mwls.getDiffData();
            // save values on tags
            CHKERR moab.tag_set_data(tag_approx_handles[t], &*nit, 1,
                                     &*vals.data().begin());
            for (int d = 0; d != 3; d++) {
            CHKERR moab.tag_set_data(tag_approx_handles_diff(d, t), &*nit, 1,
                                       &diff_vals(d, 0));
            }
          }
        }
      }
    }
    // save meshes
    CHKERR moab.write_file("out_mwls.h5m");
    // CHKERR moab.write_file("out_mwls.h5m", "MOAB", "", &meshset, 1);
    CHKERR mwls.mwlsMoab.write_file("out_mwls_moab.h5m", "MOAB");
  }
  CATCH_ERRORS;

  MoFEM::Core::Finalize();
}





