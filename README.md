# Moving weighted least squares module 

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.556465.svg)](https://doi.org/10.5281/zenodo.556465)


### Running code with density from CT scan (.mhd file)

./mwls_approx -my_file 3tet_mesh.h5m -my_residual_stress_block 1 \
-my_mwls_approx_file cube_for_test.mhd -mwls_dm 3.0 \
-mwls_number_of_base_functions 10 \
-scaleMHD .1 -threshold 3.5


* `-my_residual_stress_block 1` meshset on which data is approximated
* `-mwls_dm 3.0` controls size of influence radius for MWLS approximation
* `-mwls_number_of_base_functions` number of base functions 1 - constant, 4 - linear and 10 - quadratic
* `-trim` trims the points outside the volume 
* `-density_param` coefficients a,b (y=ax+b) for linear relationship CT data - density
* `-threshold` minimum value of CT data taken into consideration (in order to neglect values for surroundings)
* `-scaleMHD` set the scale of MHD file

### Running code with internal stresses (in fracture module)


Read internal stresses form med file, results are stored in out.h5m:
```
../tools/read_med -med_file examples/stresses_ELGA.rmed
```

If you have time steps in med file use option `-med_time_step 1`.

Run analysis:
```
mpirun -np 4 ./crack_propagation -my_file ./examples/brick_slice.cub  \
-my_max_post_proc_ref_level 1 \
-my_order 2 -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels 2 \
-my_ref 0 -my_geom_order 1 -my_ref_order 0 \
-my_residual_stress_block 1 \
-my_mwls_approx_file out.h5m -my_internal_stress_name SIGP \
-mwls_dm 1.25 -mwls_number_of_base_functions 4
```

* `-mwls_dm 1.25` controls size of influence radius for MWLS approximation
* `-mwls_number_of_base_functions` number of base functions 1 - constant, 4 - linear and 10 - quadratic


```
./tools/read_med -med_file examples/internal_stress/sslv116a.rmed -med_time_step 1
```

```
mpirun -np 4 ./crack_propagation -my_file sslv116a_mesh.h5m \
-my_max_post_proc_ref_level 0 \
-mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels 2 \
-my_ref 0 -my_geom_order 1 -my_ref_order 1 -my_residual_stress_block 1 \
-my_mwls_approx_file out.h5m  -mwls_dm 4 -mwls_number_of_base_functions 4 \
-my_internal_stress_name RESUMECASIEF_ELGA -load_scale 1
```
